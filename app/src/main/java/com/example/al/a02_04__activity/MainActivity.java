package com.example.al.a02_04__activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) { //при создании Активности (переопределение метода суперкласса)
        Log.i("Тест", "Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(this, "Я создан!", Toast.LENGTH_SHORT).show();
    }

//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) { //при восстановлении состояния UI
//        Log.i("Тест", "RestoreInstanceState");
//        super.onRestoreInstanceState(savedInstanceState);
//    }

    @Override
    protected void onStart() { //когда Активность стала видимой
        Log.i("Тест", "Start");
        super.onStart();
    }

    @Override
    protected void onResume() { //при восстановлении из неактивного состояния
        Log.i("Тест", "Resume");
        super.onResume();
    }

//    @Override
//    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) { //перед выходом из активного состояния при сохранении состояния
//        Log.i("Тест", "SaveInstanceState");
//        super.onSaveInstanceState(outState, outPersistentState);
//    }

    @Override
    protected void onPause() { //перед выходом из активного состояния
        Log.i("Тест", "Pause");
        super.onPause();
    }

    @Override
    protected void onStop() { //перед выходом из видимого состояния
        Log.i("Тест", "Stop");
        super.onStop();
    }

    @Override
    protected void onDestroy() { //перед уничтожением активности
        Log.i("Тест", "Destroy");
        super.onDestroy();
    }
}
